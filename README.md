# Anki from youtube
## What does the program do?

The program "Anki from youtube" allows you to create flashcard with audio from youtube (and also mp3 files). You can for example create flashcards from songs and add fragments from those songs containing the unknown word.

## Prerequisites 

- Download python https://www.python.org/downloads/ (if you download it with Windows installer remember to check Add Python to PATH, otherwise it has to be done manually)
- Download, install and add to PATH FFMPEG (video with explanations https://www.youtube.com/watch?v=r1AtmY-RMyQ)
- Install python libraries, go to CMD and paste (python has to be already installed and added to the PATH):<br /><br />
_pip install xlrd <br />
pip install xlwt <br />
pip install pytube <br />
pip install pydub_ <br />


## How to use the program


In order to use the program you have to download youtube_to_anki.exe and template.xlt<br />
Create a copy of the template and start adding new words to the file.<br />
There are two types of columns: those marked in red are main fields and those in blue are additional.
<img src="photos/template.png" width="750">
<br />Fields in the input file:
- Definition: a definition of the word in your language
- Word: a word in the target language
- Phrase: a phrase with this word
- Youtube link/input audio file: URL to youtube video or name of mp3 input file (can be put with or without ".mp3" extension)
- Min & Sec: The time marking the appearance of the word in the youtube video (or mp3 file)
- Comment 1, Comment 2 & Comment Front: Any additional comments
<img src="photos/template_example.png" width="750">
<br /> Example<br /><br />
If the excel file is already filled you can proceed to the youtube_to_anki.exe file. Open it. You should see a GUI as below.
<img src="photos/GUI.png" width="500">
<br />First, you have to click "Choose input file" and find your file. Then click "Choose storage folder". In this folder, all files will be stored (downloaded files from youtube, fragments, output files). It is good to choose an empty folder (you can use later the same folder or always create a new one). In the chosen folder subfolders will be created automatically.<br />

<img src="photos/GUI_after1.png" width="450">
<br /> GUI after a correct extraction and subfolders created in the chosen folder
<br /> The next stage is to download the sound from youtube, or copy it from .mp3 files (those files have to be placed in audio_input subfolder). Downloaded sound is stored in "audio" subfolder.<br />


<img src="photos/download_from_youtube.png" width="300">
<br />If there had been any problems with download the information is shown  below the progress bar (you can still proceed to the next step if some words hadn't been correctly downloaded, they are omitted).<br />

Creating fragments: Choose the length of the audio fragment and then click "Create fragments". Fragments are stored in "fragments" subfolder. The "output_with_problems" file is generated and placed into "output" subfolder if there had been and problems with creating fragments (for example in case of unsuccesfull download from youtube). You can proceed to the next step (those unsuccesful words will be omitted in next steps). If you see a message "Fragments created succesfully" below processing bar in the 3rd section it means that file "output_with_problems" hasn't been generated.<br />
<img src="photos/creating_fragments.png" width="300"><br />
Both actions (downloading sound and creating fragments) can be stopped by clicking the button "Stop everything" on the bottom of GUI.<br />
<img src="photos/generate_output.png" width="300"><br />
By clicking on checkbuttons you can decide how will your output file look like. The additional comments can be added and phrases with gaps deleted. Output file is created in the "output" subfolder. The file is called "importfile\[date+hour].txt". 

<br />
<img src="photos/example_importfile.png" width="600"><br />
Example of the output file generated.<br />


## Importing words to Anki

First you need to find "collection.media" folder on your computer. It is a folder where Anki stores its multimedia (more info: https://docs.ankiweb.net/files.html). You have to copy audio files from the "fragments" subfolder to that folder. Otherwise you won't be able to import the flashcards with audio to Anki.<br /><br />

Then click "manage note types" in Anki like in the picture below.
<br /><img src="photos/manage_note_types.png" width="300"><br />

Create a new note type and name it:
<br /><img src="photos/note_types_add.png" width="750"><br />

Select your note type and click "Fields...". Then add and rename fields like below:
<br /><img src="photos/fields.png" width="750"><br />
Then click save. Select your note type again and click "Cards..."<br />
Select "Definition" and "Phrase with a gap" and add it to the front of the card.
<br /><img src="photos/Front_note.png" width="500"><br />
Do the same action for the "Back Template" with remaining fields (Word, phrase, audio).<br />
You can change the style of the fields like on the picture below (adding bold for example):

<br /><img src="photos/customize_notes.png" width="500"><br />
You can directly copy it from here (remember to edit {{...}} if you have named them differently).<br />
**Front:**<br /><br />
```
<div style='font-family: Arial; font-weight:bold;font-size: 20px;'>{{Definition}}</div>
<div style='font-family: Arial; font-size: 20px;'>{{Phrase with a gap}}</div>
```
<br />**Back:**<br /><br />
```
{{FrontSide}}
<hr id=answer>
<div style='font-family: Arial; font-weight:bold;font-size: 20px;'>{{Word}}</div>
<div style='font-family: Arial; font-size: 20px;'>{{Phrase}}</div>
<div style='font-family: Arial; font-size: 20px;'>{{Audio}}</div>
```
<br />Now you can start importing your file. Click import and choose file created by anki_to_python.exe.
<br /><img src="photos/import_notes.png" width="500"><br />
Then choose your previously created note type and deck (you can create a new deck or choose an already existing one).
<br /><img src="photos/import_notes2.png" width="500"><br />
After a successful import you should see a pop-up like below.
<br /><img src="photos/succecfull_import.png" width="500"><br />
WELL DONE!!! You've added your flashcards to Anki.
## Problems with import to Anki
Somethimes there is a problem with importing flashcards.
<br /><img src="photos/test_import.png" width="500"><br />
Try to import test_import.txt by choosing basic type and creating a test deck. Then try again with your regular import file (choose again correct note type and your deck).
## Additional fields
You can modify output file generated by anki_to_python.exe by checking and unchecking boxes.
<br /><img src="photos/changing_import.png" width="500"><br />
Remember that you have to change note type in Anki to correspond to import file. You can clone already created note type in "Manage Note Types" and add or delete a field.
<br /><img src="photos/clone_note_type.png" width="500"><br />
## Other resources
I recommend checking Anki extensions (for example https://www.youtube.com/watch?v=ckJM1fio0uQ)
